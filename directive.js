/**
 * Created by Kapil on 8/3/2016.
 */
angular.module('pascalTriangle')
.directive('drawPascal',[function(){
        return {
            link:function(scope){
                scope.generatePascalTriangle = function(number){
                    scope.pascalTriangle = [];
                    if(number > 0){
                        scope.pascalTriangle[0] = [1];
                    }

                    for (var row = 1; row < number; row++){
                        scope.pascalTriangle[row] = [1];
                        for (var col = 1; col < row; col++) {
                            scope.pascalTriangle[row][col] = scope.pascalTriangle[row-1][col] + scope.pascalTriangle[row-1][col-1];
                        }
                        scope.pascalTriangle[row].push(1);
                    }
                };

                scope.$watch('number',function(newValue){
                    if(!isNaN(newValue)){
                        scope.generatePascalTriangle(newValue);
                    }
                });

            },
            scope:{
                number:'='
            },
            template:[
                "<table>",
                "<tr ng-repeat='row in pascalTriangle track by $index' ng-class=\"{'red':$index%4 == 0, 'blue':$index%4 == 1, 'orange':$index%4 == 2,'green':$index%4 == 3}\">",
                    "<td ng-repeat='column in row track by $index'>{{column}}</td>",
                "</tr>",
                "</table>"
            ].join("")
        }
    }
    ]);