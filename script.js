/**
 * Created by Kapil on 8/3/2016.
 */
angular.module('pascalTriangle',['ngStorage'])
.controller('HomeController',['$localStorage',function($localStorage){
    var vm = this;
    vm.$storage = $localStorage;
    
    return vm;
}]);